# Actix FS
_Asyncronous filesystem operations for actix-based systems_

## Usage

```rust
use std::io::SeekFrom;

#[actix_rt::main]
async fn main() -> Result<(), anyhow::Error> {
    let file = actix_fs::open("tests/read.txt").await?;
    let (file, position) = actix_fs::seek(file, SeekFrom::Start(7)).await?;
    let bytes = actix_fs::read_bytes(file).await?;

    assert!(position == 7);
    assert!(bytes.as_ref() == b"World!\n");
    Ok(())
}
```

### Contributing
Feel free to open issues for anything you find an issue with. Please note that any contributed code will be licensed under the AGPLv3.

### License

Copyright © 2020 Riley Trautman

pict-rs is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

pict-rs is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. This file is part of pict-rs.

You should have received a copy of the GNU General Public License along with pict-rs. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
